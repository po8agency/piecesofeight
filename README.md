## Requirements

Note: in all cases below where you see "$" symbol, this refers to a command that must be run using a command line tool e.g. Command Prompt (windows).

NodeJS


Gulp

$ npm install -g gulp

## Getting Started

$ cd "path\theme" e.g. "E:\Localhost\blueprint.local\app\public\wp-content\themes\blueprint"
$ npm install
$ gulp

## Gulp SFTP Deploy

Create a new file JSON file named '.server.json'. Add the following:

```
{
  "sftp": {
    "host": "ip or hostname",
    "path": "/var/www/html/wp-content/themes/blackbeard",
    "port": "22",
    "user": "sftp username",
    "pass": "sftp password"
  }
}
```

Note: .server.json is ignored with both GIT pushes and SFTP Deploys.

$ gulp deploy
